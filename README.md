Proyecto java 1: Mad race
F-Zero, es una saga de nintendo conocido por sus carreras de alta velocidad, sus singulares personajes y
naves, la dificultad de juego y su acabado gráfico. F-Zero fue el primero de su clase, y más tarde fue imitado
por infinidad de juegos del mismo estilo.
Mad race es una variante inspirada en F-Zero, en la cual se simulara una carrera de naves de manera lineal.

 
 Para esto el usuario podra:
 
*  Dar inicio a la carrera.
*  Reiniciar la carrera.

 Para el correcto funcionamiento de este programa se recomienda como requisito previo:

*  java 11.
*  Linux OS en cualquiera de sus distribuciones.

Este programa se desarrollo en:

*  eclipce.

 Este programa se desarrollo con leguaje:

*  java.

Este programa fue desarrollo por:

*  Nicolas Avila.

agradecimiento a Jorge Giraldo autor de proyectoCarreraCaballos. culla solucion fue implementada como base para este trabajo.

NOTA: es una vercion incompleta, por lo que puede fallar.
