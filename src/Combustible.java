
public class Combustible {
	
	private double multiplicador_velocidad, cantidad_combustible;
	private int tipo_combustible;
	
	Combustible(int cantidad_combustible, int tipo_combustible) {
		this.tipo_combustible = tipo_combustible;
		this.cantidad_combustible = cantidad_combustible;
	}
	
	public double getCantidad_combustible() {
		return cantidad_combustible;
	}
	public double getMultiplicador_velocidad() {
		return multiplicador_velocidad;
	}
	public void setMultiplicador_velocidad() {
		this.multiplicador_velocidad = (this.getTipo_combustible() + 1)/3;
	}
	public int getTipo_combustible() {
		return tipo_combustible;
	}
}

