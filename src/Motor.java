
public class Motor {
	private int peso_motor;
	private double v_max, integridad_motor, desgaste, nivel_combustible;
	
	Motor(int peso_motor,double v_max ,double integridad_motor, int tipo_combustible) {
		
		this.peso_motor = peso_motor;
		this.v_max = v_max;
		this.integridad_motor = integridad_motor;	
		this.desgaste = integridad_motor * 0.05;
		
	}
	public int getPeso_motor() {
		return peso_motor;
	}
	public double getVelocidad_max() {
		return v_max;
	}
	public void setVelocidad_max(double v_max) {
		this.v_max = v_max;
	}
	public double getIntegridad_motor() {
		return integridad_motor;
	}
	public void setIntegridad_motor(double integridad_motor) {
		this.integridad_motor = integridad_motor;
	}
	public double getDesgaste() {
		return desgaste;
	}
	public void setDesgaste(double tasa_desgaste) {
		this.desgaste = tasa_desgaste;
	}
	public void Estado_motor() {
		this.setIntegridad_motor(this.getIntegridad_motor() - this.getDesgaste());
	}
	public double getNivel_combustible() {
		return nivel_combustible;
	}
	public void setNivel_combustible(double nivel_combustible) {
		this.nivel_combustible = nivel_combustible;
	}
	public void Estado_combustible() {
		this.setNivel_combustible(this.getNivel_combustible() - 1);
	}
	
}
