import java.util.ArrayList;

public class Nave{
	private double integridad_nave, desgaste, a, v, v_max;
	private int tipo_motor, peso_nave, centro_masa, codigo, X, Y, d;
    private long tiempo;
    private String nombre;
	private ArrayList<Propulsor> propulsores = new ArrayList<Propulsor>();
	private ArrayList<Turbina> turbinas = new ArrayList<Turbina>();
	
	Nave(int tipo_motor, int cantidad_motor, int tipo_combustible, double integridad_nave, int peso_nave, String nombre,
			int codigo, int X, int Y, double v, long tiempo)  {
		if(tipo_motor == 1) {
			this.setTipo_motor(tipo_motor);
			for(int i=1;i < cantidad_motor + 1;i++) {
				Turbina t = new Turbina(100, 20, 100, tipo_combustible);
				turbinas.add(t);
			}
			this.v_max = turbinas.get(0).getVelocidad_max() * cantidad_motor;
			this.peso_nave = peso_nave + (turbinas.get(0).getPeso_motor() * cantidad_motor);
		}
		if(tipo_motor == 2) {
			this.setTipo_motor(tipo_motor);
			for(int i=1;i < cantidad_motor + 1;i++) {
				Propulsor p = new Propulsor(150, 30, 100, tipo_combustible);
				propulsores.add(p);
			}
			this.v_max = propulsores.get(0).getVelocidad_max() * cantidad_motor;
			this.peso_nave = peso_nave + (propulsores.get(0).getPeso_motor() * cantidad_motor);
		}
		this.integridad_nave = integridad_nave;
		this.desgaste = this.getIntegridad_nave() * 0.05;
		
		this.codigo = codigo;
        this.X = X;
        this.Y = Y;
        this.tiempo = tiempo;
        this.nombre = nombre;
		
	}
	public double getVelocidad() {
		return v;
	}
	public void setVelocidad() {
		if (this.tipo_motor == 1) {
			this.v = Math.floor(Math.random()*(this.getVelocidad_max() -10)+10) * (this.getTurbina().get(0).getIntegridad_motor()/100);
		}else {
			this.v = Math.floor(Math.random()*(this.getVelocidad_max() -10)+10) * (this.getPropulsor().get(0).getIntegridad_motor()/100);
		}
		
	}
	public double getVelocidad_max() {
		return v_max;
	}

	public void setVelocidad_max(double v_max) {
		this.v_max = v_max;
	}

	public int getPeso_nave() {
		return peso_nave;
	}

	public void setPeso_nave(int peso_nave) {
		this.peso_nave = peso_nave;
	}

	public int getCentro_masa() {
		return centro_masa;
	}

	public void setCentro_masa(int centro_masa) {
		this.centro_masa = centro_masa;
	}

	public double getIntegridad_nave() {
		return integridad_nave;
	}

	public void setIntegridad_nave(double integridad_nave) {
		this.integridad_nave = integridad_nave;
	}

	public double getDesgaste() {
		return desgaste;
	}

	public void setDesgaste(double desgaste) {
		this.desgaste = desgaste;
	}
	
	public void Estado_nave() {
		this.setIntegridad_nave(this.getIntegridad_nave() - this.getDesgaste());
	}

	public double getAceleracion() {
		return a;
	}
	public void setAceleracion() {
		this.a = this.getVelocidad() / System.currentTimeMillis();
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getX() {
		return X;
	}
	public void setX(int x) {
		X = x;
	}
	public int getY() {
		return Y;
	}
	public void setY(int y) {
		Y = y;
	}
	public long getTiempo() {
		return tiempo;
	}
	public void setTiempo(long tiempo) {
		this.tiempo = tiempo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDistancia() {
		return d;
	}
	public void setDistancia() {
		this.d = (int)this.getVelocidad() * (int)System.currentTimeMillis();
	}
	public int getTipo_motor() {
		return tipo_motor;
	}
	public void setTipo_motor(int tipo_motor) {
		this.tipo_motor = tipo_motor;
	}
	public ArrayList<Turbina> getTurbina() {
		return turbinas;
	}
	public ArrayList<Propulsor> getPropulsor() {
		return propulsores;
	}
}
