import java.util.Random;

public class Pista {
    
    static Nave[] Naves;
    static Random rnd;

    
    static public void reiniciar(){
     //Vel
        Naves[0].setVelocidad();
        Naves[1].setVelocidad();
        Naves[2].setVelocidad();
        Naves[3].setVelocidad();
        
        //avances
        Naves[0].setDistancia();
        Naves[1].setDistancia();
        Naves[2].setDistancia();
        Naves[3].setDistancia();
        
        Naves[0].setAceleracion();
        Naves[1].setAceleracion();
        Naves[2].setAceleracion();
        Naves[3].setAceleracion();
    
    }//
    
    static public void registrarNaves(){
    	rnd = new Random();
        Naves = new Nave[4];
        
        Naves[0] = new Nave(rnd.nextInt(2) + 1, rnd.nextInt(20) + 1, rnd.nextInt(5) + 1, 100, 200, "-", 0, 0, 0, 0, 0);
        Naves[1] = new Nave(rnd.nextInt(2) + 1, rnd.nextInt(20) + 1, rnd.nextInt(5) + 1, 100, 200, "-", 0, 0, 0, 0, 0);
        Naves[2] = new Nave(rnd.nextInt(2) + 1, rnd.nextInt(20) + 1, rnd.nextInt(5) + 1, 100, 200, "-", 0, 0, 0, 0, 0);
        Naves[3] = new Nave(rnd.nextInt(2) + 1, rnd.nextInt(20) + 1, rnd.nextInt(5) + 1, 100, 200, "-", 0, 0, 0, 0, 0);
        //ingreso de datos
        //codigos
        Naves[0].setCodigo(101);
        Naves[1].setCodigo(102);
        Naves[2].setCodigo(103);
        Naves[3].setCodigo(104);
        //nombres
        Naves[0].setNombre("skull-1");
        Naves[1].setNombre("skull-2");
        Naves[2].setNombre("skull-3");
        Naves[3].setNombre("skull-4");
        //CoordX
        Naves[0].setX(FmrInicio.lblAuto1.getLocation().x);
        Naves[1].setX(FmrInicio.lblAuto2.getLocation().x);
        Naves[2].setX(FmrInicio.lblAuto3.getLocation().x);
        Naves[3].setX(FmrInicio.lblAuto4.getLocation().x);
        //CoordX
        Naves[0].setY(FmrInicio.lblAuto1.getLocation().y);
        Naves[1].setY(FmrInicio.lblAuto2.getLocation().y);
        Naves[2].setY(FmrInicio.lblAuto3.getLocation().y);
        Naves[3].setY(FmrInicio.lblAuto4.getLocation().y);
        //Vel
        Naves[0].setVelocidad();
        Naves[1].setVelocidad();
        Naves[2].setVelocidad();
        Naves[3].setVelocidad();
        
        //avances
        Naves[0].setDistancia();
        Naves[1].setDistancia();
        Naves[2].setDistancia();
        Naves[3].setDistancia();
    
        //aceleracion
        Naves[0].setAceleracion();
        Naves[1].setAceleracion();
        Naves[2].setAceleracion();
        Naves[3].setAceleracion();
        //
        Naves[0].setTiempo(0);
        Naves[1].setTiempo(0);
        Naves[2].setTiempo(0);
        Naves[3].setTiempo(0);
        
        //
    }//
    
    
    static void iniciar(){
        
        hiloUno h1= new hiloUno(Naves[0].getNombre());
        h1.start();
        
        hiloUno h2= new hiloUno(Naves[1].getNombre());
        h2.start();
        
        hiloUno h3= new hiloUno(Naves[2].getNombre());
        h3.start();
        
        hiloUno h4= new hiloUno(Naves[3].getNombre());
        h4.start();
    }//iniciar           
            
    
    
    }//class

