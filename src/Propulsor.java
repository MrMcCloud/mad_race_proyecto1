
public class Propulsor extends Motor{
	private Combustible combustible;
	
	Propulsor(int peso_motor, double velocidad_max, double integridad_motor, int tipo_combustible) {
		super(peso_motor, velocidad_max, integridad_motor, tipo_combustible);
		Combustible combustible = new Combustible(100, tipo_combustible);
		this.setNivel_combustible(combustible.getCantidad_combustible());
		this.Buff_velocidad(combustible.getTipo_combustible());
		this.Debuff_desgaste(combustible.getTipo_combustible());
	}
	public void Buff_velocidad(int tipo_combustible) {
		if(tipo_combustible == 1 || tipo_combustible == 2 || tipo_combustible == 5) {
			this.setVelocidad_max(this.getVelocidad_max() + (this.getVelocidad_max() * 0.10));
		}	
	}
	public void Debuff_desgaste(int tipo_combustible) {
		if(tipo_combustible != 1 || tipo_combustible != 2 || tipo_combustible != 5) {
			this.setDesgaste(this.getDesgaste() + (this.getIntegridad_motor() * 0.05));
		}
	}
	public Combustible getCombustible() {
		return combustible;
	}
	public void setCombustible(Combustible combustible) {
		this.combustible = combustible;
	}
}
