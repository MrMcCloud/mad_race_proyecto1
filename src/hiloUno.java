
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;
import java.util.concurrent.TimeUnit;
import javax.swing.plaf.SliderUI;


public class hiloUno extends Thread{

    
    public hiloUno(String name) {
        this.setName(name);
    }//constr
    
    @SuppressWarnings("deprecation")
	public void run(){
        
        
    
        if(getName().equals("skull-1")){
            
            long ti=System.currentTimeMillis();
            
            for (int j = 0; j < 99; j++) {
                try {
                	Pista.Naves[0].setVelocidad();
                	Pista.Naves[0].setDistancia();
                    FmrInicio.lblAuto1.setLocation(FmrInicio.lblAuto1.getLocation().x+Pista.Naves[0].getDistancia(),FmrInicio.lblAuto1.getLocation().y);
                    //System.out.println(this.getName()+":"+FmrInicio.lblAuto1.getLocation()+" -- "+FmrInicio.lblLinea.getLocation().x);
                    
                    
                    sleep((int)Pista.Naves[1].getVelocidad());
                    
                    if (Pista.Naves[0].getTipo_motor() == 1) {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                        FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[0].getTurbina().get(0).Estado_motor();
                        FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[0].getTurbina().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[0].getTurbina().get(0).Estado_combustible();
                        FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[0].getTurbina().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[0].Estado_nave();
                        FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[0].getIntegridad_nave()+"%");
                        FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[0].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[0].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                    }else {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                    	FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[0].getPropulsor().get(0).Estado_motor();
                    	FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[0].getPropulsor().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[0].getPropulsor().get(0).Estado_combustible();
                    	FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[0].getPropulsor().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[0].Estado_nave();
                    	FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[0].getIntegridad_nave()+"%");
                    	FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[0].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[0].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                    }
                    if(FmrInicio.lblAuto1.getLocation().x>=FmrInicio.lblLinea.getLocation().x){
                        long tf=System.currentTimeMillis();
                        long tt= tf-ti;
                        Pista.Naves[0].setTiempo(tt);
                        FmrInicio.txtR.append("\n"+this.getName()+":"+Pista.Naves[0].getTiempo()+" ms");
                        j=99;
                        }else{
                        j++;
                    }//else
                } //for
                catch (InterruptedException ex) {
                    Logger.getLogger(hiloUno.class.getName()).log(Level.SEVERE, null, ex);
                }
              
              
             
        }//for
            
            FmrInicio.lblAuto1.setLocation(FmrInicio.lblAuto1.getLocation().x,FmrInicio.lblAuto1.getLocation().y);
             
             this.stop();
        }//if
              
        
        if(getName().equals("skull-2")){
            long ti=System.currentTimeMillis();
            for (int j = 0; j < 99; j++) {
                try {
                	Pista.Naves[1].setVelocidad();
                	Pista.Naves[1].setDistancia();
                    FmrInicio.lblAuto2.setLocation(FmrInicio.lblAuto2.getLocation().x+Pista.Naves[1].getDistancia(),FmrInicio.lblAuto2.getLocation().y);
                    //System.out.println(this.getName()+":"+FmrInicio.lblAuto1.getLocation()+" -- "+FmrInicio.lblLinea.getLocation().x);
                    
                    sleep((int)Pista.Naves[1].getVelocidad());
                    
                    if (Pista.Naves[1].getTipo_motor() == 1) {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                        FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[1].getTurbina().get(0).Estado_motor();
                        FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[1].getTurbina().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[1].getTurbina().get(0).Estado_combustible();
                        FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[1].getTurbina().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[1].Estado_nave();
                        FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[1].getIntegridad_nave()+"%");
                        FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[1].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[1].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                    }else {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                    	FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[1].getPropulsor().get(0).Estado_motor();
                    	FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[1].getPropulsor().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[1].getPropulsor().get(0).Estado_combustible();
                    	FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[1].getPropulsor().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[1].Estado_nave();
                    	FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[1].getIntegridad_nave()+"%");
                    	FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[1].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[1].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                    }
                    
                    if(FmrInicio.lblAuto2.getLocation().x>=FmrInicio.lblLinea.getLocation().x){
                        long tf=System.currentTimeMillis();
                        long tt= tf-ti;
                        Pista.Naves[1].setTiempo(tt);
                        FmrInicio.txtR.append("\n"+this.getName()+":"+Pista.Naves[1].getTiempo()+" ms");
                        j=99;
                        }else{
                        j++;
                    }//else
                } //for
                catch (InterruptedException ex) {
                    Logger.getLogger(hiloUno.class.getName()).log(Level.SEVERE, null, ex);
                }
              
              
             
        }//for
             FmrInicio.lblAuto2.setLocation(FmrInicio.lblAuto2.getLocation().x,FmrInicio.lblAuto2.getLocation().y);        
        this.stop();
        }//if
        
        if(getName().equals("skull-3")){
            long ti=System.currentTimeMillis();
            for (int j = 0; j < 99; j++) {
                try {
                	Pista.Naves[2].setVelocidad();
                	Pista.Naves[2].setDistancia();
                    FmrInicio.lblAuto3.setLocation(FmrInicio.lblAuto3.getLocation().x+Pista.Naves[2].getDistancia(),FmrInicio.lblAuto3.getLocation().y);
                    //System.out.println(this.getName()+":"+FmrInicio.lblAuto1.getLocation()+" -- "+FmrInicio.lblLinea.getLocation().x);
                    
                    
                    sleep((int)Pista.Naves[2].getVelocidad());
                    
                    if (Pista.Naves[2].getTipo_motor() == 1) {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                        FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[2].getTurbina().get(0).Estado_motor();
                        FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[2].getTurbina().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[2].getTurbina().get(0).Estado_combustible();
                        FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[2].getTurbina().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[2].Estado_nave();
                        FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[2].getIntegridad_nave()+"%");
                        FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[2].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[2].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                    }else {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                    	FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[2].getPropulsor().get(0).Estado_motor();
                    	FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[2].getPropulsor().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[2].getPropulsor().get(0).Estado_combustible();
                    	FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[2].getPropulsor().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[2].Estado_nave();
                    	FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[2].getIntegridad_nave()+"%");
                    	FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[2].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[2].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                    }
                    
                    if(FmrInicio.lblAuto3.getLocation().x>=FmrInicio.lblLinea.getLocation().x){
                        long tf=System.currentTimeMillis();
                        long tt= tf-ti;
                        Pista.Naves[2].setTiempo(tt);
                        FmrInicio.txtR.append("\n"+this.getName()+":"+Pista.Naves[2].getTiempo()+" ms");
                        j=99;
                        }else{
                        j++;
                    }//else
                } //for
                catch (InterruptedException ex) {
                    Logger.getLogger(hiloUno.class.getName()).log(Level.SEVERE, null, ex);
                }
              
              
             
        }//for
             FmrInicio.lblAuto3.setLocation(FmrInicio.lblAuto3.getLocation().x,FmrInicio.lblAuto3.getLocation().y);                
        this.stop();
        }//if
        
        if(getName().equals("skull-4")){
            long ti=System.currentTimeMillis();
          for (int j = 0; j < 99; j++) {
                try {
                	Pista.Naves[3].setVelocidad();
                	Pista.Naves[3].setDistancia();
                    FmrInicio.lblAuto4.setLocation(FmrInicio.lblAuto4.getLocation().x+Pista.Naves[3].getDistancia(),FmrInicio.lblAuto4.getLocation().y);
                    //System.out.println(this.getName()+":"+FmrInicio.lblAuto1.getLocation()+" -- "+FmrInicio.lblLinea.getLocation().x);
                    
                    
                    sleep((int)Pista.Naves[3].getVelocidad());
                    
                    if (Pista.Naves[3].getTipo_motor() == 1) {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                        FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[3].getTurbina().get(0).Estado_motor();
                        FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[3].getTurbina().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[3].getTurbina().get(0).Estado_combustible();
                        FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[3].getTurbina().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[3].Estado_nave();
                        FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[3].getIntegridad_nave()+"%");
                        FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[3].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[3].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                        
                    }else {
                    	FmrInicio.txtR.append("\n-----------------------------------------------------");
                    	FmrInicio.txtR.append("\n"+this.getName());
                    	Pista.Naves[3].getPropulsor().get(0).Estado_motor();
                    	FmrInicio.txtR.append("\n integridad del motor: "+Pista.Naves[3].getPropulsor().get(0).getIntegridad_motor()+"%");
                    	Pista.Naves[3].getPropulsor().get(0).Estado_combustible();
                    	FmrInicio.txtR.append("\n estado del combustible: "+Pista.Naves[3].getPropulsor().get(0).getNivel_combustible()+"L");
                    	Pista.Naves[3].Estado_nave();
                    	FmrInicio.txtR.append("\n integridad de la nave: "+Pista.Naves[3].getIntegridad_nave()+"%");
                    	FmrInicio.txtR.append("\n velocidad de la nave: "+Pista.Naves[3].getVelocidad()+"Km/h");
                        FmrInicio.txtR.append("\n aceleracion de la nave: "+Pista.Naves[3].getIntegridad_nave()+"Km/s*s");
                        FmrInicio.txtR.append("\n-----------------------------------------------------");
                    }
                    
                    if(FmrInicio.lblAuto4.getLocation().x>=FmrInicio.lblLinea.getLocation().x){
                        long tf=System.currentTimeMillis();
                        long tt= tf-ti;
                        Pista.Naves[3].setTiempo(tt);
                        FmrInicio.txtR.append("\n"+this.getName()+":"+Pista.Naves[3].getTiempo()+" ms");
                        j=99;
                        }else{
                        j++;
                    }//else
                } //for
                catch (InterruptedException ex) {
                    Logger.getLogger(hiloUno.class.getName()).log(Level.SEVERE, null, ex);
                }
              
              
             
        }//for
             FmrInicio.lblAuto4.setLocation(FmrInicio.lblAuto4.getLocation().x,FmrInicio.lblAuto4.getLocation().y);                
        this.stop();
        }//if
        
    }//run

    
   

        
    }//class